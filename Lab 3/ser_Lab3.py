'''
@file ser_Lab3.py
@brief This file does serial communication from the PC to the Nucleo.
@details The code here will open a serial port to the Nucleo and communicate
with it to send commands and recieve data. After the go command from the computer,
and a button press on the nucleo, the data will be read, interpreted, and printed as a plot
and saved as a .csv file.
The full code for ser_Lab3.py can be found at https://bitbucket.org/bbons/me-405/src/master/Lab%203/ser_Lab3.py .

@author Ben Bons

@date Last Updated: February 3, 2021

@package Lab_3
@brief This package contains all the files for Lab 3.
@details The full code for main_Lab3.py can be found at https://bitbucket.org/bbons/me-405/src/master/Lab%203/main_Lab3.py .
The full code for ser_Lab3.py can be found at https://bitbucket.org/bbons/me-405/src/master/Lab%203/ser_Lab3.py .
'''
import serial
import time
from matplotlib import pyplot
import csv 

## The serial line object that data is transmitted over
ser = serial.Serial(port='COM3', baudrate = 115200, timeout = 1)
try:
    ## The input from the user
    inp = input('Enter \'G\' to go: ') 
    ser.write(inp.encode('ascii'))
    while ser.in_waiting == 0:
        time.sleep(0.1)
    ## The string array of data that represents the data from the ADC, in raw digital format
    dataStrgs = ser.readline().decode('ascii').strip()[12:-2].split(', ')
    #print(dataStrgs)
    ## The data in its full length from the ADC
    data_raw = []
    for strg in dataStrgs:
        # Convert string to int and then convert raw ADC value to voltage
        data_raw.append(float(strg)*3.3/4095)
    # Clean up the data - focus around the step response
    ## The approximate index where the step response starts
    idx_1 = -1
    
    ## The approximate index where the step response ends
    idx_2 = -1
    
    ## The iterable that is the indexes of data_raw
    idxs = iter(range(len(data_raw)))
    # This loop looks through the raw data and finds the approximate indexes where the step response occurs
    while idx_2 == -1:
        try:
            ## \cond
            i = next(idxs)
            ## \endcond
            if data_raw[i] < (4000*3.3/4095) and idx_1 == -1:
                idx_1 = i
            if data_raw[i] < (10*3.3/4095) and idx_2 == -1:
                idx_2 = i
        except StopIteration:
            idx_2 = i
    # This section widens the selection to include some data before and after the step response        
    idx_1 -= 20
    if idx_1 < 0:
        idx_1 = 0
        
    idx_2 += 20
    if idx_2 >= len(data_raw):
        idx_2 = len(data_raw)-1
    
    ## The trimmed data from the ADC in volts
    data = data_raw[idx_1:idx_2]
    pyplot.figure()
    
    ## The times in microseconds of each data point
    time = list(range(len(data)))
    pyplot.plot(time,data)
    pyplot.xlabel('Time [microseconds]')
    pyplot.ylabel('C13 Voltage [Volts]')
    pyplot.savefig('TimeVsVoltage.png')
    pyplot.show()
    ## \cond
    # Write to csv file
    with open('VoltageVsTime.csv', 'w', newline = '') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['Time [microseconds]'] + ['C13 Voltage [Volts]'])
        ## \endcond
        for n in range(len(time)):
            writer.writerow([time[n]] + [data[n]])
except:
    ser.__del__()
    raise
ser.__del__()