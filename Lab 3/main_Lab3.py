'''
@file main_Lab3.py
@brief Creates a class that reads from the ADC
@details The full code for main_Lab3.py can be found at https://bitbucket.org/bbons/me-405/src/master/Lab%203/main_Lab3.py .

@author Ben Bons
@date Last Updated February 03, 2021

@package Lab_3
'''

import pyb
import array

class read_ADC:
    '''
    @brief This class reads from the ADC using the run() function
    @details This class is used to wrap up variables and create a function that waits for
    a button press and then reads the ADC input from that. 
    '''
    
    ## The ADC object for reading the voltage values
    ADC = pyb.ADC(pyb.Pin.cpu.A0)
    
    ## The timer object used for counting on the buffer
    tim = pyb.Timer(2,freq = 1000000)
    
    ## The buffer that the ADC writes to
    buf = array.array('H', (0 for index in range(200)))
    
    ## The LED that turns on and off to indicate when the buffer is being written to
    led = pyb.Pin(pyb.Pin.cpu.A5, mode = pyb.Pin.OUT_PP)
    
    ## A flag variable used for internal control
    flag = 0
    
    ## The bus used for UART communication
    bus = pyb.UART(2)

    def run():
        '''
        @brief This method runs an infinite loop to read from the ADC
        @details When called, this function will loop infinitely. It waits for a G
        on the serial line. When that happens, the ADC will continuously fill the buffer. 
        The data will be transmitted when a step responce is detected
        '''
        
        while True:
            char = read_ADC.bus.readchar()
            # Executes if the character is g or G
            if char == 71 or char == 103:
                read_ADC.flag = 1
                # turns on LED during data collection
                read_ADC.led.value(1)
                while read_ADC.flag == 1:
                    read_ADC.ADC.read_timed(read_ADC.buf,read_ADC.tim)
                    # if a step response happens, this if statement breaks the loop
                    if (read_ADC.buf[0] - read_ADC.buf[-1]) >=100:
                        read_ADC.flag = 0
                read_ADC.led.value(0)
                # write the data down the bus
                read_ADC.bus.write(str(read_ADC.buf).encode('ascii') + '\r\n')
                # reset the buffer
                for i in range(len(read_ADC.buf)):
                    read_ADC.buf[i] = 0
                    
                
                    