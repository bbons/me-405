'''
@file reaction_Time_Lab2.py
@brief Creates a reaction time program for the Nucleo
@details This program tests the user's reaction time. An LED will light up, and the user must press the blue
button. The reaction time will be printed after the press. After a keyboard interrupt the average reaction time
will be printed and the program will exit. The LED will turn on after 2-3 seconds, and stay on for 1 second. If
the user does not press the button before a second passes, the LED turns off and no reaction time is recorded. The full
code can be found at https://bitbucket.org/bbons/me-405/src/master/Lab%202/reaction_Time_Lab2.py .

@author Ben Bons
@date Last Updated: January 27, 2020
'''

import sys
import pyb
import random
import micropython
micropython.alloc_emergency_exception_buf(150)

## The timer object used for measuring reaction time
tim = pyb.Timer(2, prescaler = 79, period = 0x7FFFFFFF)

## The LED that is turned on and off
led = pyb.Pin(pyb.Pin.cpu.A5, mode = pyb.Pin.OUT_PP)

## The array of all the reaction times
t_array = []

## The time that the button is pressed and the light turns off. This must be predefined so that the interrupt functions correctly.
lightOff = -1

def onButtonPress(button):
    '''
    @brief The callback function for the button press
    @details This function will set the lightOff variable if the LED is on
    '''
    global lightOff
    
    lightOff = tim.counter() ##Coded in this way to avoid delays in measuring reaction time
    if led.value() == 0:
        lightOff = -1
    else:
        led.value(0)
    #print (lightOff) 

def enterTime():
    '''
    @brief This function corrects time deltas and appends the delta to a list
    @details The function subtracts lightOff from lightOn, and in the case of an overflow, the time is 
    corrected by adding the period of the timer. 
    '''
    global lightOff
    if lightOff>=0:
        #print(lightOff)
        del_count = lightOff - lightOn
        print('Reaction Time: {:.3} seconds'.format(del_count/1000000))
        if del_count < 0:
            del_count += 0x7FFFFFFF + 1
        lightOff = -1
        t_array.append(del_count)
## The pin that the button is on
button = pyb.Pin(pyb.Pin.cpu.C13)

## The button interrupt that is called when the button is pressed.
buttonInt = pyb.ExtInt(button, mode = pyb.ExtInt.IRQ_FALLING, pull = pyb.Pin.PULL_UP, callback = onButtonPress)

## The time that the light turns on.
lightOn = 0

def run():
    '''
    @brief This file starts the reaction time program.
    '''
    
    try:
        while True:
            global lightOn
            randNum = random.randint(2000,3000) 
            pyb.delay(randNum)
            led.value(1)
            lightOn = tim.counter()
            pyb.delay(1000)
            led.value(0)
            enterTime()
    except KeyboardInterrupt:
        try:
            avg = sum(t_array)/len(t_array)
        except ZeroDivisionError:
            print('No reaction time to record')
        else:
            print ('The average reaction time was {:.3} seconds'.format(avg/1000000))
        
        sys.exit()
        
if __name__ == '__main__':
    run()
