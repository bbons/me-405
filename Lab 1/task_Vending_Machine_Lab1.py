'''
@file task_Vending_Machine_Lab1.py
@brief This file contains the task_Vending_Machine class.

@author Ben Bons
@date Last Modified: January 20, 2021
'''

import keyboard
import time

class task_Vending_Machine:
    '''
    @brief Defines the finite state machine for the vending machine
    @details This class defines a virtual vending machine. The class will take in money
    and output drinks based on user input.
    '''
    
    ## The initialization state
    S0_INIT = 0
    
    ## The waiting for payment state
    S1_WAIT_FOR_PAYMENT = 1
    
    ## The waiting for drink selection state
    S2_WAIT_FOR_SELECTION = 2
    
    ## The drink outputting state
    S3_OUTPUT_DRINK = 3
    
    ## The state when a drink is selected when not enough balance is in the machine
    S4_INSUFFICIENT_FUNDS = 4
    
    ## The state where change is given
    S5_GIVE_CHANGE = 5
    
    ## The drink that has been selected
    drink = None
    
    ## The dictionary of soda prices
    sodas = {'c': 100, 'p': 120, 's': 85, 'd': 110}
    
    ## The dictionary of drink names
    soda_names = {'c': 'Cuke', 'p': 'Popsi', 's': 'Spryte', 'd': 'Dr. Pupper'}
    
    def __init__(self, interval):
        '''
        @brief Constructor for the task object
        @details This constructor sets all the variables that define the behavior
        for the vending machine
        
        @param interval The float time in seconds between iterations of run()
        '''
        ## The state of the finite state machine
        self.state = self.S0_INIT
        
        ## The interval between executions of run()
        self.interval = interval
        
        ## The time when the task is initialized
        self.start_time = time.time()
        
        ## The next time when run() will run
        self.next_time = self.start_time + self.interval
        
        ## The flag variable for controlling key input
        self.key = 0
    
        ## The balance in the machine
        self.balance = 0
        
        ## The drink that has been selected
        self.drink = None
        
    def run(self):
        '''
        @brief Runs one iteration of the finite state machine
        @details This code will read keyboard inputs and then use them to control the 
        vending machine. The number keys will input money. 0 is a penny, up to 9 being a 
        $100 bill. C gives cuke, S gives Spryte, D gives Dr. Popper, and P gives Popsi.
        Pressing E will eject all money in the machine and then reset the machine.
        '''
        if (time.time()>=self.next_time):
            keyboard.on_release(self.on_keyrelease)
            if self.state == self.S0_INIT:
                print('Initializing Vending Machine')
                self.switchState(self.S1_WAIT_FOR_PAYMENT)
                self.printBal()
            elif self.state == self.S1_WAIT_FOR_PAYMENT:
                
                if keyboard.is_pressed('0') and self.key ==0:
                    print('Penny Inserted')
                    self.balance +=1
                    self.printBal()
                    self.key = 1
                    
                if keyboard.is_pressed('1') and self.key == 0:
                    print('Nickle Inserted')
                    self.balance += 5
                    self.printBal()
                    self.key = 1
                    
                if keyboard.is_pressed('2') and self.key == 0:
                    print('Dime Inserted')
                    self.balance += 10
                    self.printBal()
                    self.key = 1
                    
                if keyboard.is_pressed('3') and self.key == 0:
                    print('Quarter Inserted')
                    self.balance += 25
                    self.printBal()
                    self.key = 1
                
                if keyboard.is_pressed('4') and self.key == 0:
                    print('1 Dollar Bill Inserted')
                    self.balance += 100
                    self.printBal()
                    self.key = 1
                    
                if keyboard.is_pressed('5') and self.key == 0:
                    print('5 Dollar Bill Inserted')
                    self.balance += 500
                    self.printBal()
                    self.key = 1
                    
                if keyboard.is_pressed('6') and self.key == 0:
                    print('10 Dollar Bill Inserted')
                    self.balance += 1000
                    self.printBal()
                    self.key = 1
                    
                if keyboard.is_pressed('7') and self.key == 0:
                    print('20 Dollar Bill Inserted')
                    self.balance += 2000
                    self.printBal()
                    self.key = 1
                    
                if keyboard.is_pressed('8') and self.key == 0:
                    print('50 Dollar Bill Inserted')
                    self.balance += 5000
                    self.printBal()
                    self.key = 1
                    
                if keyboard.is_pressed('9') and self.key == 0:
                    print('100 Dollar Bill Inserted')
                    self.balance += 10000
                    self.printBal()
                    self.key = 1
                    
                if keyboard.is_pressed('e') and self.key == 0:
                    print('Ejecting Remaining Balance')
                    self.balance = 0
                    self.switchState(self.S0_INIT)
                    self.key = 1
                    
                if keyboard.is_pressed('c') and self.key == 0:
                    if self.balance >= self.sodas['c']:
                        self.switchState(self.S3_OUTPUT_DRINK)
                        self.drink = 'c'
                        print('Dispensing a {}'.format(self.soda_names[self.drink]))
                        self.balance -= self.sodas[self.drink]
                    else:
                        self.switchState(self.S4_INSUFFICIENT_FUNDS)
                    self.key = 1
                        
                if keyboard.is_pressed('p') and self.key == 0:
                    if self.balance >= self.sodas['p']:
                        self.switchState(self.S3_OUTPUT_DRINK)
                        self.drink = 'p'
                        print('Dispensing a {}'.format(self.soda_names[self.drink]))
                        self.balance -= self.sodas[self.drink]
                    else:
                        self.switchState(self.S4_INSUFFICIENT_FUNDS)
                    self.key = 1
                        
                if keyboard.is_pressed('s') and self.key == 0:
                    if self.balance >= self.sodas['s']:
                        self.switchState(self.S3_OUTPUT_DRINK)
                        self.drink = 's'
                        print('Dispensing a {}'.format(self.soda_names[self.drink]))
                        self.balance -= self.sodas[self.drink]
                    else:
                        self.switchState(self.S4_INSUFFICIENT_FUNDS)
                    self.key = 1
                        
                if keyboard.is_pressed('d') and self.key == 0:
                    if self.balance >= self.sodas['d']:
                        self.switchState(self.S3_OUTPUT_DRINK)
                        self.drink = 'd'
                        print('Dispensing a {}'.format(self.soda_names[self.drink]))
                        self.balance -= self.sodas[self.drink]
                    else:
                        self.switchState(self.S4_INSUFFICIENT_FUNDS)
                    self.key = 1
            
            elif self.state == self.S2_WAIT_FOR_SELECTION:
                pass
            
            elif self.state == self.S3_OUTPUT_DRINK:
                print('Kachunk!')
                self.switchState(self.S1_WAIT_FOR_PAYMENT)
                self.printBal()
                
            elif self.state == self.S4_INSUFFICIENT_FUNDS:
                print('Insufficient Funds - insert more money')
                self.switchState(self.S1_WAIT_FOR_PAYMENT)
                self.printBal()
                
            elif self.state == self.S5_GIVE_CHANGE:
                pass
                    
            self.next_time += self.interval
        
    def on_keyrelease(self, pressed):
        '''
        @brief Callback for keypress action
        @details This function is used to perform only 1 action per keypress, by controlling
        a flag variable that allows additional commands only after a key has been released.
        '''
        self.key = 0
        
    def switchState(self, new_state):
        '''
        @brief Switches the state of the finite state machine
        '''
        self.state = new_state
    def printBal(self):
        '''
        @brief Prints the formatted current balance in the vending machine
        '''
        print('Current Balance : ${:.2f}'.format(self.balance/100))
## @cond       
if __name__ == '__main__':
    a = task_Vending_Machine(0.001)
    while True:
        a.run()
        
## @endcond