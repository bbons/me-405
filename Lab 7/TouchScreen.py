'''
@file TouchScreen.py
@brief This file contains the TouchScreen driver for a resistive touch screen
@details The file is a driver for the touchscreen on our balancing platform. The full code can be found at
https://bitbucket.org/bbons/me-405/src/master/Lab%207/TouchScreen.py .
'''
import micropython
import pyb
import utime
import gc

micropython.alloc_emergency_exception_buf(200)
class TouchScreen:
    '''
    @brief A driver class for a resistive touch screen.
    @details This class assumes that the screen is XxX meters, and that the ADC has a resolution of 12 bits. The class will take X, Y, and Z
    measurements off of the touchscreen, relative to center.
    '''
    
    #m
    
    def __init__(self, x_p, x_m, y_p, y_m, L, W, zero):
        '''
        @brief Constructor for the TouchScreen object
        @details This constructor takes in pin references for the TouchScreen object. References must be given in a pyb.Pin.cpu.XX format.
        @param x_p The pin connected to the X+ terminal on the touchscreen
        @param x_m The pin connected to the X- terminal on the touchscreen
        @param y_p The pin connected to the Y+ terminal on the touchscreen
        @param y_m The pin connected to the Y- terminal on the touchscreen
        @param L The X differenee in ADC values representing 100 mm in the X direction - used to make a calibration constant
        @param W The Y width in ADC values representing 100 mm in the Y direction - used to make a calibration constant
        @param zero A 2-tuple representing the ADC values of the center of the touchscreen
        '''
        ## The pin reference for the X+ terminal
        self.x_p = x_p
        
        ## The pin reference for the X- terminal
        self.x_m = x_m
        
        ## The pin reference for the Y+ terminal
        self.y_p = y_p
        
        ## The pin reference for the Y- terminal 
        self.y_m = y_m
        
        ## The pin that gets set logic high during position measurement
        self.pin_high = None
        
        ## The pin that gets set logic low during the position measurement
        self.pin_low = None
        
        ## The pin that the ADC reads on during position measurement
        self.pin_read = None
        
        ## The pin that is set to a High-Z input state during position measurement
        self.pin_highz = None
        
        self._volts = 0
        
        self._pos = 0
        
        self._x = 0
        
        self._y = 0
        
        self._z = False
        
        
        
        #Experimentally determined calibration constants
        self._slopeX = 100/L
        self._slopeY = 100/W
        self._zero = zero
        gc.collect()
        self.get_X()
        
        
    @micropython.native
    def get_X(self):
        '''
        @brief Gets the X position off of the touchscreen
        @details This reads the raw ADC value and then converts that value to mm. This is done using a linear relationship between relative
        position and the voltage read, with an offset being used to put a 0 position at the center of the touchscreen. Negative and positive
        values can change with how the screen is installed, so it would be wise for a user to test the program to see where the negative and
        positive is for their installation.
        @return The X position in mm
        '''
        
        self.pin_high = pyb.Pin(self.x_p, mode = pyb.Pin.OUT_PP)
        self.pin_low = pyb.Pin(self.x_m, mode = pyb.Pin.OUT_PP)
        self.pin_read = pyb.ADC(self.y_p)
        self.pin_highz = pyb.Pin(self.y_m, mode = pyb.Pin.IN)
        self.pin_high.value(1)
        self.pin_low.value(0)
        utime.sleep_us(4)
        self._volts =  self.pin_read.read()
        self._pos = int((self._volts - self._zero[0]) * self._slopeX)
        return self._pos
        
    @micropython.native
    def get_Y(self):
        '''
        @brief Gets the Y position off of the touchscreen
        @details This reads the raw ADC value and then converts that value to mm. This is done using a linear relationship between relative
        position and the voltage read, with an offset being used to put a 0 position at the center of the touchscreen. Negative and positive
        values can change with how the screen is installed, so it would be wise for a user to test the program to see where the negative and
        positive is for their installation.
        @return The Y position in mm
        '''
        self.pin_high = pyb.Pin(self.y_p, mode = pyb.Pin.OUT_PP)
        self.pin_low = pyb.Pin(self.y_m, mode = pyb.Pin.OUT_PP)
        self.pin_read = pyb.ADC(self.x_p)
        self.pin_highz = pyb.Pin(self.x_m, mode = pyb.Pin.IN)
        self.pin_high.value(1)
        self.pin_low.value(0)
        utime.sleep_us(4)
        self._volts =  self.pin_read.read()
        self._pos = int((self._volts - self._zero[1]) * self._slopeY)
        return self._pos
    
    @micropython.native
    def get_Z (self) -> bool:
        '''
        @brief Tells whether or not anything is in contact with the screen
        @details This function returns True when something is touching the screen, and false when something is not
        @return A boolean representing whether or not something is touching the screen.
        '''
        self.pin_high = pyb.Pin(self.y_p, mode = pyb.Pin.OUT_PP)
        self.pin_low = pyb.Pin(self.x_m, mode = pyb.Pin.OUT_PP)
        self.pin_read = pyb.ADC(self.y_m)
        self.pin_highz = pyb.Pin(self.x_p, mode = pyb.Pin.IN)
        self.pin_high.value(1)
        self.pin_low.value(0)
        utime.sleep_us(4)
        return self.pin_read.read() < 4050
    
    @micropython.native
    def get_All(self):
        '''
        @brief Returns a tuple of positional data
        @details This function calls get_X(), get_Y(), and get_Z() internally. A tuple of these values is then returned.
        @return A tuple of form (X,Y,Z) where X and Y are integers in mm of an objects location on the screen, and Z is a boolean representing whether or not something is touching the screen.
        '''
        self._x = self.get_X()
        self._y = self.get_Y()
        self._z = self.get_Z()
        return (self._x, self._y, self._z)
## @cond        
if __name__ == "__main__":
    # #create screen object
    screen = TouchScreen(pyb.Pin.cpu.A6, pyb.Pin.cpu.A7, pyb.Pin.cpu.A1, pyb.Pin.cpu.A0, 2056, 3160, (2015,2045))
    
    print("\nTouch at X = 0 mm, Y = 0 mm as marked on board")
    while not screen.get_Z():
        pass
    (x,y,z) = screen.get_All()
    print('Screen touched at X: {} mm, Y: {} mm'.format(x,y))
    while screen.get_Z():
        pass
    
    print("\nTouch at X = 30 mm, Y = 0 mm as marked on board")
    while not screen.get_Z():
        pass
    (x,y,z) = screen.get_All()
    print('Screen touched at X: {} mm, Y: {} mm'.format(x,y))
    while screen.get_Z():
        pass
    
    print("\nTouch at X = 0 mm, Y = 30 mm as marked on board")
    while not screen.get_Z():
        pass
    (x,y,z) = screen.get_All()
    print('Screen touched at X: {} mm, Y: {} mm'.format(x,y))
    while screen.get_Z():
        pass
    
    print("\nProfile get_X()")
    total = 0
    for n in range(100):
        start = utime.ticks_us()
        x = screen.get_X()
        delta = utime.ticks_diff(utime.ticks_us(), start)
        total += delta
        gc.collect()
    print("Average Time: {} us".format(total/100))
    
    print("\nProfile get_Y()")
    total = 0
    for n in range(100):
        start = utime.ticks_us()
        x = screen.get_Y()
        delta = utime.ticks_diff(utime.ticks_us(), start)
        total += delta
        gc.collect()
    print("Average Time: {} us".format(total/100))
    
    print("\nProfile get_Z()")
    total = 0
    for n in range(100):
        start = utime.ticks_us()
        x = screen.get_Z()
        delta = utime.ticks_diff(utime.ticks_us(), start)
        total += delta
        gc.collect()
    print("Average Time: {} us".format(total/100))
    
    
    print("\nProfile get_All()")
    total = 0
    for n in range(100):
        start = utime.ticks_us()
        x = screen.get_All()
        delta = utime.ticks_diff(utime.ticks_us(), start)
        total += delta
        gc.collect()
    print("Average Time: {} us".format(total/100))
## @endcond    