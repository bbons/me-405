'''
@file homework1.py
@brief Contains the function getChange()
@details The function in this file is for homework one. It takes a tuple of the
bills and coins used for paying a specified price, and outputs the bills and coins
needed for change. The full code can be found at https://bitbucket.org/bbons/me-405/src/master/Homework%201/homework1.py 

@author Ben Bons
@date Last Updated: January 6, 2021
'''
import math

def getChange(price, payment):
    '''
    @brief Takes a price and a payment and calculates bills and coins
    @details This function takes in a double for the price of the object, and the
    payment in a tuple of the coins and bills that are used. The return is then a tuple of
    the bills and coins to be given as change. The format for both the payment and change 
    tuples is as follows: (pennies, nickels, dimes, quarters, 1 dollar bills, 5 dollar bills
                           10 dollar bills, 20 dollar bills, 50 dollar bills, 100 dollar bills)
    
    @param price The price of the object being paid for
    @param payment The tuple of the coins and bills being used for payment. It follows
    the format seen above. The tuple can be any length up to and equal to 10. Any tuple
    longer than 10 will cause an error.
    
    @return The 10-tuple of change according to the format above.
    '''
    ## The coins and bills that are used to calculate change
    bills = (0.01, 0.05, 0.10, 0.25, 1, 5, 10, 20, 50, 100)
    price = round(price,2)
    money = 0
    # Error handling if payment only contains 1 digit
    try:
        length = len(payment)
    except:
        length = 1
        payment = (payment, 0)
    
    if length > len(bills):
        raise ValueError('Too many values in payment tuple')
    
    for x in range(length):
        money += payment[x]*bills[x]
    money = round(money,2)    
    
    change = money-price

    if change < 0:
        print('Not enough paid, pay more!')
        return None
    change_list = []
    for x in range(len(bills)-1, -1, -1):
        n = math.floor(change/bills[x])
        change -= bills[x]*n
        change = round(change,2)
        #print (change)
        change_list.append(n)
    
    change_list.reverse()    
    change_tuple = tuple(change_list)
    
    return change_tuple